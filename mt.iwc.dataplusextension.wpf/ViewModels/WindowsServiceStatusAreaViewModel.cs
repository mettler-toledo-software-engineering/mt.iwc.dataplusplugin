﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.wpf.Stores;
using MVVMEssentials.ViewModels;

namespace mt.iwc.dataplusextension.wpf.ViewModels;

public class WindowsServiceStatusAreaViewModel : ViewModelBase
{
    
    public WindowsServiceStatusAreaViewModel(WindowsServiceStore windowsServiceStore)
    {
        ServiceState = windowsServiceStore.DataPlusExtensionWindowsService.ServiceState;
        _windowsServiceStore = windowsServiceStore;
        _windowsServiceStore.ServiceStateChanged += WindowsServiceStoreOnServiceStateChanged;
        
    }

    private void WindowsServiceStoreOnServiceStateChanged(DataPlusExtensionWindowsServiceState serviceState)
    {
        ServiceState = serviceState;
    }

    private DataPlusExtensionWindowsServiceState _serviceState;
    private readonly WindowsServiceStore _windowsServiceStore;

    public DataPlusExtensionWindowsServiceState ServiceState
    {
        get { return _serviceState; }
        set
        {
            _serviceState = value;
            OnPropertyChanged(nameof(ServiceState));
        }
    }

    public override void Dispose()
    {
        _windowsServiceStore.ServiceStateChanged -= WindowsServiceStoreOnServiceStateChanged;
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using mt.iwc.dataplusextension.application.Services;
using mt.iwc.dataplusextension.wpf.Commands;
using mt.iwc.dataplusextension.wpf.Stores;
using MVVMEssentials.ViewModels;

namespace mt.iwc.dataplusextension.wpf.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    public ICommand RefreshPortListCommand { get; }
    
    public MainWindowViewModel(GlobalMessageViewModel globalMessageViewModel, ScaleConnectionListingViewModel scaleConnectionListingViewModel, WindowsServiceMessageAreaViewModel windowsServiceMessageAreaViewModel, WindowsServiceStatusAreaViewModel windowsServiceStatusAreaViewModel)
    {
        RefreshPortListCommand = new RefreshPortListCommand(this);
        RefreshPortListCommand.Execute(null);
        
        GlobalMessageViewModel = globalMessageViewModel;
        ScaleConnectionListingViewModel = scaleConnectionListingViewModel;
        WindowsServiceMessageAreaViewModel = windowsServiceMessageAreaViewModel;
        WindowsServiceStatusAreaViewModel = windowsServiceStatusAreaViewModel;
    }

    private List<string> _portNames = new ();

    public List<string> PortNames 
    {
        get { return _portNames; }
        set
        {
            _portNames = value;
            OnPropertyChanged(nameof(PortNames));
        }
    }

    public GlobalMessageViewModel GlobalMessageViewModel { get; }
    public ScaleConnectionListingViewModel ScaleConnectionListingViewModel { get; }

    public WindowsServiceMessageAreaViewModel WindowsServiceMessageAreaViewModel { get; }
    public WindowsServiceStatusAreaViewModel WindowsServiceStatusAreaViewModel { get; }
}

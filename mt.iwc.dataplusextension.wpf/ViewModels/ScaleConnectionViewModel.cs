﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Services.Communication;
using mt.iwc.dataplusextension.wpf.Commands;

using mt.iwc.dataplusextension.wpf.Stores;
using MVVMEssentials.ViewModels;

namespace mt.iwc.dataplusextension.wpf.ViewModels;

public class ScaleConnectionViewModel :ViewModelBase
{


    public ICommand TestConnectionCommand { get; }

    public ICommand DeleteConnectionCommand { get; }

    public ScaleConnectionViewModel(ScaleConnectionStore scaleConnectionStore, GlobalMessageStore messageStore, ConnectionConfiguration connectionConfiguration, WindowsServiceStore windowsServiceStore)
    {
        SelectedDataPlusComPort = connectionConfiguration.DataPlusComPort;
        ScaleName = connectionConfiguration.ScaleName;
        SelectedScaleComPort = connectionConfiguration.ScaleComPort;
        SelectedScaleType = connectionConfiguration.Scale;
        ConnectionId = connectionConfiguration.ConnectionId;
        ConnectionState = ConnectionState.NewConnection;
        windowsServiceStore.ConnectionUpdateReceived += WindowsServiceStoreOnConnectionUpdateReceived;

        TestConnectionCommand = new TestConnectionCommand(this, scaleConnectionStore, messageStore, windowsServiceStore);
        DeleteConnectionCommand = new DeleteConnectionCommand(scaleConnectionStore, messageStore, this, windowsServiceStore);
        _comPorts = new ObservableCollection<string>(scaleConnectionStore.GetSerialPortNames());

        PropertyChanged += OnPropertyChanged;
    }

    private void OnPropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName != nameof(HasBeenTested))
        {
            HasBeenTested = false;
        }
    }

    private void WindowsServiceStoreOnConnectionUpdateReceived(ConnectionState state, Guid id)
    {
        if (id == ConnectionId)
        {
            ConnectionState = state;
        }
        
    }

    private ConnectionState _connectionState;

    public ConnectionState ConnectionState
    {
        get { return _connectionState; }
        set
        {
            _connectionState = value;
            OnPropertyChanged(nameof(ConnectionState));
        }
    }


    private readonly ObservableCollection<string> _comPorts;
    public IEnumerable<string> ComPorts => _comPorts;

    private readonly ObservableCollection<ScaleType> _scaleTypes = new() { ScaleType.BBK};
    public IEnumerable<ScaleType> ScaleTypes => _scaleTypes;

    private object? _selectedScaleComPort;

    public object? SelectedScaleComPort
    {
        get { return _selectedScaleComPort; }
        set
        {
            _selectedScaleComPort = value;
            OnPropertyChanged(nameof(SelectedScaleComPort));
        }
    }

    private object? _selectedDataPlusComPort;

    public object? SelectedDataPlusComPort
    {
        get { return _selectedDataPlusComPort; }
        set
        {
            _selectedDataPlusComPort = value;
            OnPropertyChanged(nameof(SelectedDataPlusComPort));
        }
    }

    private string? _scaleName;

    public string? ScaleName
    {
        get { return _scaleName; }
        set
        {
            _scaleName = value;
            OnPropertyChanged(nameof(ScaleName));
        }
    }

    private bool _hasBeenTested;

    public bool HasBeenTested
    {
        get { return _hasBeenTested; }
        set
        {
            _hasBeenTested = value;
            OnPropertyChanged(nameof(HasBeenTested));
        }
    }

    private object _selectedScaleType;

    public object SelectedScaleType
    {
        get { return _selectedScaleType; }
        set
        {
            _selectedScaleType = value;
            OnPropertyChanged(nameof(SelectedScaleType));
        }
    }

    public Guid ConnectionId { get; private set; }

    public override void Dispose()
    {

        base.Dispose();
    }
}

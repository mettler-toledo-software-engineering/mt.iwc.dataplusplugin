﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.wpf.Commands;
using mt.iwc.dataplusextension.wpf.Stores;
using MVVMEssentials.ViewModels;

namespace mt.iwc.dataplusextension.wpf.ViewModels;

public class ScaleConnectionListingViewModel : ViewModelBase
{
    private readonly ScaleConnectionStore _scaleConnectionStore;
    private readonly GlobalMessageStore _messageStore;
    private ObservableCollection<ScaleConnectionViewModel>? _scaleConnections = new();
    public IEnumerable<ScaleConnectionViewModel>? ScaleConnections => _scaleConnections;

    public ICommand AddScaleConnectionViewCommand { get; }
    public ICommand StartWindowsServiceCommand { get; }
    public ICommand StopWindowsServiceCommand { get; }
    public ICommand SaveScaleConnectionsCommand { get; }

    public ScaleConnectionListingViewModel(ScaleConnectionStore scaleConnectionStore, GlobalMessageStore messageStore, WindowsServiceStore windowsServiceStore)
    {
        AddScaleConnectionViewCommand = new AddScaleConnectionViewCommand(this, scaleConnectionStore, messageStore, windowsServiceStore);
        _scaleConnectionStore = scaleConnectionStore;
        _scaleConnectionStore.ConnectionDeleted += ScaleConnectionStoreOnConnectionDeleted;
        _messageStore = messageStore;
        StartWindowsServiceCommand = new StartWindowsServiceCommand(messageStore, windowsServiceStore);
        StopWindowsServiceCommand = new StopWindowsServiceCommand(messageStore, windowsServiceStore);
        SaveScaleConnectionsCommand = new SaveScaleConnectionsCommand(scaleConnectionStore, messageStore, windowsServiceStore);
        LoadConnections();

    }

    private void ScaleConnectionStoreOnConnectionDeleted(ScaleConnectionViewModel viewModel)
    {
        _scaleConnections?.Remove(viewModel);
    }

    private void LoadConnections()
    {
        if (_scaleConnectionStore?.ScaleConnections != null)
        {
            foreach (var connection in _scaleConnectionStore.ScaleConnections)
            {
                AddScaleConnectionViewCommand.Execute(connection);
            }
        }
    }

    public void AddConnectionView(ScaleConnectionViewModel connectionViewModel)
    {
        _scaleConnections?.Insert(0, connectionViewModel);
    }
    
}

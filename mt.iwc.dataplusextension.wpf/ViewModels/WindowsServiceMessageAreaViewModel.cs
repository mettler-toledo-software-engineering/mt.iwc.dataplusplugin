﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.wpf.Stores;
using MVVMEssentials.ViewModels;

namespace mt.iwc.dataplusextension.wpf.ViewModels;

public class WindowsServiceMessageAreaViewModel : ViewModelBase
{
    private readonly WindowsServiceStore _windowsServiceStore;

    public WindowsServiceMessageAreaViewModel(WindowsServiceStore windowsServiceStore)
    {
        _windowsServiceStore = windowsServiceStore;
        _windowsServiceStore.ServerStatusReceived += WindowsServiceStoreOnServerStatusReceived;
        
    }

    private void WindowsServiceStoreOnServerStatusReceived(string message)
    {
        ServerMessages.Insert(0, new WindowsServiceMessageViewModel(message));

        if (ServerMessages.Count > 10)
        {
            ServerMessages.RemoveAt(10);
        }
    }

    public ObservableCollection<WindowsServiceMessageViewModel> ServerMessages { get; } = new();

    public override void Dispose()
    {
        _windowsServiceStore.ServerStatusReceived -= WindowsServiceStoreOnServerStatusReceived;
        base.Dispose();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.wpf.Stores;
using MVVMEssentials.ViewModels;
using System.Timers;

namespace mt.iwc.dataplusextension.wpf.ViewModels;

public class GlobalMessageViewModel : ViewModelBase
{
    private readonly GlobalMessageStore _globalMessageStore;

    public string Message => _globalMessageStore.CurrentMessage;
    public bool HasMessage => !string.IsNullOrWhiteSpace(_globalMessageStore.CurrentMessage);
    public bool IsErrorMessage => _globalMessageStore.CurrentMessageType == GlobalMessageStore.MessageType.Error;
    public bool IsInfoMessage => _globalMessageStore.CurrentMessageType == GlobalMessageStore.MessageType.Info;
    public bool IsSuccessMessage => _globalMessageStore.CurrentMessageType == GlobalMessageStore.MessageType.Success;
    public bool IsNoneMessage => _globalMessageStore.CurrentMessageType == GlobalMessageStore.MessageType.None;
    private Timer? _timer;

    public GlobalMessageViewModel(GlobalMessageStore globalMessageStore)
    {
        _globalMessageStore = globalMessageStore;
        _globalMessageStore.CurrentMessageChanged += OnCurrentMessageChanged;
        StartTimer();
    }

    private void StartTimer()
    {
        _timer = new Timer();
        _timer.Interval = 3000;
        _timer.Enabled = true;
        _timer.AutoReset = false;
        _timer.Elapsed += OnTimerElapsed;
    }

    private void OnTimerElapsed(object? sender, ElapsedEventArgs e)
    {
        _globalMessageStore.UpdateMessage(string.Empty, GlobalMessageStore.MessageType.None);
    }

    private void OnCurrentMessageChanged()
    {
        if (IsNoneMessage == false)
        {
            _timer?.Start();
        }
        OnPropertyChanged(nameof(Message));
        OnPropertyChanged(nameof(HasMessage));
        OnPropertyChanged(nameof(IsErrorMessage));
        OnPropertyChanged(nameof(IsInfoMessage));
        OnPropertyChanged(nameof(IsSuccessMessage));
        OnPropertyChanged(nameof(IsNoneMessage));
    }

    public override void Dispose()
    {
        _globalMessageStore.CurrentMessageChanged -= OnCurrentMessageChanged;
        if (_timer != null)
        {
            _timer.Elapsed -= OnTimerElapsed;
        }
    }
}

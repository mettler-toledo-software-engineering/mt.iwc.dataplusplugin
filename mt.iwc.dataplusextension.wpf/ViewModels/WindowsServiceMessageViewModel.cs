﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVVMEssentials.ViewModels;

namespace mt.iwc.dataplusextension.wpf.ViewModels;

public class WindowsServiceMessageViewModel : ViewModelBase
{
    private string _serverMessage;

    public string ServerMessage
    {
        get { return _serverMessage; }
        set
        {
            _serverMessage = value;
            OnPropertyChanged(nameof(ServerMessage));
        }
    }

    public WindowsServiceMessageViewModel(string serverMessage)
    {
        ServerMessage = serverMessage;
    }
}

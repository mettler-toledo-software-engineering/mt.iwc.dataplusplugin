﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace mt.iwc.dataplusextension.wpf.Stores;

public class GlobalMessageStore
{
    private readonly ILogger _logger;

    public enum MessageType
    {
        Success,
        Info,
        Error,
        None
    }

    public GlobalMessageStore(ILogger logger)
    {
        CurrentMessage = "Applikation Startet...";
        CurrentMessageType = MessageType.Info;
        _logger = logger;
    }

    public string CurrentMessage { get; private set; }

    public event Action? CurrentMessageChanged;

    public MessageType CurrentMessageType { get; private set; }

    public void UpdateMessage(string message, MessageType messageType, Exception? exception = null)
    {
        CurrentMessage = message;
        CurrentMessageType = messageType;
        CurrentMessageChanged?.Invoke();

        if (CurrentMessageType == MessageType.Error)
        {
            _logger.Error(exception, message);
        }

        if (CurrentMessageType == MessageType.Info)
        {
            _logger.Debug(message);
        }
    }
}

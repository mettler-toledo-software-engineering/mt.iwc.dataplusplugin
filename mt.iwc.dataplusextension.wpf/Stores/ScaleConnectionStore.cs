﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Persistence;
using mt.iwc.dataplusextension.application.Services.Communication;
using mt.iwc.dataplusextension.application.Services.Factories;
using mt.iwc.dataplusextension.wpf.ViewModels;

namespace mt.iwc.dataplusextension.wpf.Stores;

public class ScaleConnectionStore
{
    private readonly IScaleConnectionJsonService _jsonService;
    private readonly ISerialToSerialConnectionFactory _connectionFactory;
    private readonly IGetSerialPortsService _getSerialPortsService;

    public List<ConnectionConfiguration>? ScaleConnections { get; private set; }

    public event Action<ScaleConnectionViewModel?> ConnectionDeleted;
    public event Action ScaleConnectionViewModelChanged; 
    private readonly List<ScaleToDataPlusConnection> _runningConnections = new ();
    public readonly List<ScaleConnectionViewModel> ScaleConnectionViewModels = new();


    public ScaleConnectionStore(IScaleConnectionJsonService jsonService, ISerialToSerialConnectionFactory connectionFactory, IGetSerialPortsService getSerialPortsService)
    {
        _jsonService = jsonService;
        _connectionFactory = connectionFactory;
        _getSerialPortsService = getSerialPortsService;

        ScaleConnections = new List<ConnectionConfiguration>();
        ScaleConnections = _jsonService.LoadScaleConnections();
    }


    public void DeleteConnection(Guid connectionId)
    {
        ScaleConnections?.RemoveAll(c => c.ConnectionId == connectionId);
        _runningConnections.RemoveAll(c => c.Configuration?.ConnectionId == connectionId);
        var result = ScaleConnectionViewModels.Find(vieModel => vieModel.ConnectionId == connectionId);
        if (result != null)
        {
            ScaleConnectionViewModels.Remove(result);
            ConnectionDeleted?.Invoke(result);
        }
        
        SaveScaleConnections();
        
    }

    public void AddScaleConnection(ConnectionConfiguration connection)
    {
        ScaleConnections?.RemoveAll(c => c.ConnectionId == connection.ConnectionId);
        ScaleConnections?.Add(connection);
    }

    public void AddScaleConnectionViewModel(ScaleConnectionViewModel viewModel)
    {
        ScaleConnectionViewModels.Add(viewModel);
        viewModel.PropertyChanged += ViewModelOnPropertyChanged;
        viewModel.ConnectionState = ConnectionState.NewConnection;
    }

    private void ViewModelOnPropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        ScaleConnectionViewModelChanged?.Invoke();
    }

    //public void AddRunningConnection(ScaleToDataPlusConnection runningConnection)
    //{
    //    _runningConnections.RemoveAll(c => c.Configuration.ConnectionId == runningConnection.Configuration.ConnectionId);
    //    _runningConnections.Add(runningConnection);
    //}

    public void SaveScaleConnections()
    {
        _jsonService.SaveScaleConnections(ScaleConnections);
    }

    public ISerialToSerialConnectionService CreateConnection()
    {
        return _connectionFactory.CreateConnectionService();
    }

    public IEnumerable<string> GetSerialPortNames()
    {
        return _getSerialPortsService.GetSerialPortNames();
    }


}

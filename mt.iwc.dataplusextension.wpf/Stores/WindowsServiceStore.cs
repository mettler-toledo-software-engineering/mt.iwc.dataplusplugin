﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Services.ClientServerMethods;
using mt.iwc.dataplusextension.application.Services.Communication;

namespace mt.iwc.dataplusextension.wpf.Stores;

public class WindowsServiceStore
{
    private readonly GlobalMessageStore _messageStore;
    private readonly IDataPlusExtensionWindowsServiceService _windowsServiceService;
    private readonly ISignalRClientService _signalRClient;

    public event Action<DataPlusExtensionWindowsServiceState> ServiceStateChanged;
    public event Action<string> ServerStatusReceived;
    public event Action<ConnectionState, Guid> ConnectionUpdateReceived;

    public WindowsServiceStore(GlobalMessageStore messageStore, IDataPlusExtensionWindowsServiceService windowsServiceService, ISignalRClientService signalRClient)
    {
        _messageStore = messageStore;
        _windowsServiceService = windowsServiceService;
        _signalRClient = signalRClient;
        _signalRClient.Connect().ContinueWith(HandleException);

        DataPlusExtensionWindowsService = _windowsServiceService.GetDataPlusExtensionWindowsService();
    }

    private void HandleException(Task task)
    {
        if (task.Exception != null)
        {
            _messageStore.UpdateMessage("Verbindung zum DataPlusExtensionService auf Port 5000 / 5001 fehlgeschlagen",GlobalMessageStore.MessageType.Error, task.Exception);
            return;
        }
        _signalRClient.ServerStatusReceived -= SignalRClientOnServerStatusReceived;
        _signalRClient.ServerStatusReceived += SignalRClientOnServerStatusReceived;

        _signalRClient.ConnectionStateReceived -= SignalRClientOnActiveScaleConnectionReceived;
        _signalRClient.ConnectionStateReceived += SignalRClientOnActiveScaleConnectionReceived;
    }

    private void SignalRClientOnActiveScaleConnectionReceived(ConnectionState state, Guid id)
    {
        ConnectionUpdateReceived?.Invoke(state, id);
    }

    private void SignalRClientOnServerStatusReceived(string message)
    {
        ServerStatusReceived?.Invoke(message);
    }

    public DataPlusExtensionWindowsService DataPlusExtensionWindowsService { get; }


    private void CheckServiceState()
    {
        DataPlusExtensionWindowsService.ServiceState = _windowsServiceService.GetDataPlusExtensionWindowsServiceState();
        ServiceStateChanged?.Invoke(DataPlusExtensionWindowsService.ServiceState);
        if (DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Starting || DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Stopping)
        {
            CheckServiceState();
        }
    }

    public void StartWindowsService()
    {
        _windowsServiceService.StartDataPlusExtensionWindowsService();
        CheckServiceState();
        _signalRClient.Connect().ContinueWith(HandleException);
    }

    public void StopWindowsService()
    {
        _windowsServiceService.StopDataPlusExtensionWindowsService();
        CheckServiceState();
    }
}

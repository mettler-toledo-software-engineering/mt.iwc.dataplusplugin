﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mt.iwc.dataplusextension.application.Persistence;
using mt.iwc.dataplusextension.application.Services;

using MVVMEssentials.Services;

namespace mt.iwc.dataplusextension.wpf.HostBuilders;

public static class AddNavigationServicesHostBuilderExtensions
{
    public static IHostBuilder AddNavigationServices(this IHostBuilder host)
    {
        host.ConfigureServices(services =>
        {

            services.AddSingleton<CloseModalNavigationService>();

        });

        return host;
    }
}

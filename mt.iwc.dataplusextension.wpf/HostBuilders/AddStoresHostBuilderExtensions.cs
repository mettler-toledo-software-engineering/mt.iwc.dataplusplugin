﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mt.iwc.dataplusextension.wpf.Stores;
using MVVMEssentials.Stores;

namespace mt.iwc.dataplusextension.wpf.HostBuilders;

public static class AddStoresHostBuilderExtensions
{
    public static IHostBuilder AddStores(this IHostBuilder host)
    {
        host.ConfigureServices(services =>
        {
            services.AddSingleton<NavigationStore>();
            services.AddSingleton<ScaleConnectionStore>();
            services.AddSingleton<GlobalMessageStore>();
            services.AddSingleton<WindowsServiceStore>();
         
        });

        return host;
    }
}

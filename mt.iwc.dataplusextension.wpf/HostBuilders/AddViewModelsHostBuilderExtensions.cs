﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mt.iwc.dataplusextension.application.Services;
using mt.iwc.dataplusextension.wpf.Stores;
using mt.iwc.dataplusextension.wpf.ViewModels;


namespace mt.iwc.dataplusextension.wpf.HostBuilders;

public static class AddViewModelsHostBuilderExtensions
{
    public static IHostBuilder AddViewModels(this IHostBuilder host)
    {
        host.ConfigureServices(services =>
        {
            services.AddSingleton<MainWindowViewModel>();
            services.AddSingleton<GlobalMessageViewModel>();
            services.AddSingleton<ScaleConnectionListingViewModel>();
            services.AddSingleton<WindowsServiceMessageAreaViewModel>();
            services.AddSingleton<WindowsServiceStatusAreaViewModel>();
            


        });

        return host;
    }
}



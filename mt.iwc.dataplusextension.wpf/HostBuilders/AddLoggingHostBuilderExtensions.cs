﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace mt.iwc.dataplusextension.wpf.HostBuilders;

public static class AddLoggingHostBuilderExtensions
{
    public static IHostBuilder AddLogging(this IHostBuilder host)
    {
        host.UseSerilog((_, loggerConfiguration) =>
        {
            loggerConfiguration
                .WriteTo.File(@"c:\DataPlusExtension\Logging\Client_Log_.txt", rollingInterval: RollingInterval.Day)
                .WriteTo.Debug()
                .MinimumLevel.Information();
        });

        return host;
    }
}
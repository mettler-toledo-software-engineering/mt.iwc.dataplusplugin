﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mt.iwc.dataplusextension.application.Persistence;
using mt.iwc.dataplusextension.application.Services.ClientServerMethods;
using mt.iwc.dataplusextension.application.Services.Communication;
using mt.iwc.dataplusextension.application.Services.Factories;
using mt.iwc.dataplusextension.application.Services.Translation;

namespace mt.iwc.dataplusextension.wpf.HostBuilders;

public static class AddServicesHostBuilderExtensions
{
    public static IHostBuilder AddServices(this IHostBuilder host)
    {
        host.ConfigureServices(services =>
        {
            services.AddSingleton<IGetSerialPortsService, GetSerialPortsService>();
            
            services.AddSingleton<ISerialToSerialConnectionFactory, SerialToSerialConnectionFactory>();
            services.AddSingleton<IDataPlusExtensionWindowsServiceService, DataPlusExtensionWindowsServiceService>();
            services.AddSingleton<ISignalRClientService>(new SignalRClientService(new HubConnectionBuilder().WithUrl("http://localhost:5000/dataPlusExtension").WithAutomaticReconnect().Build()));
            

            services.AddTransient<IScaleConnectionJsonService, ScaleConnectionJsonService>();

        });

        return host;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mt.iwc.dataplusextension.wpf.HostBuilders;
using mt.iwc.dataplusextension.wpf.ViewModels;
using Forms = System.Windows.Forms;


namespace mt.iwc.dataplusextension.wpf;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : Application
{
    private MainWindow? _mainWindow;
    private readonly Forms.NotifyIcon _notifyIcon;
    private readonly IHost _host;

    
    public App()
    {


        if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly()?.Location)).Count() > 1)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        _host = CreateHostBuilder().Build();
        _notifyIcon = new Forms.NotifyIcon();
    }

    public static IHostBuilder CreateHostBuilder(string[]? args = null)
    {
        return Host.CreateDefaultBuilder(args)
            .AddConfiguration()
            .AddStores()
            .AddServices()
            .AddNavigationServices()
            .AddViewModels()
            .AddLogging()
            .AddViews();
    }
    protected override void OnStartup(StartupEventArgs e)
    {
        _host.Start();
        _mainWindow = _host.Services.GetRequiredService<MainWindow>();
        _mainWindow.Show();
        _mainWindow.StateChanged += MainWindowOnStateChanged;
        _notifyIcon.Icon = new System.Drawing.Icon("ressources/scale.ico");
        _notifyIcon.Text = "Dataplus Scale Extension";
        _notifyIcon.Visible = true;

        _notifyIcon.Click += NotifyIconOnClick;


        base.OnStartup(e);

    }

    private void NotifyIconOnClick(object? sender, EventArgs e)
    {
        if (_mainWindow != null)
        {
            _mainWindow.Show();
            _mainWindow.WindowState = WindowState.Normal;
            _mainWindow.Activate();
        }
        
    }

    private void MainWindowOnStateChanged(object? sender, EventArgs e)
    {
        if (_mainWindow?.WindowState == WindowState.Minimized)
        {
            _mainWindow.Hide();
        }
    }

    protected override void OnExit(ExitEventArgs e)
    {
        _notifyIcon.Dispose();
        base.OnExit(e);
    }
}

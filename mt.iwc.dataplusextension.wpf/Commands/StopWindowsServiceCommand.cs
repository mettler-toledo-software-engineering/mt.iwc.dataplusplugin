﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.wpf.Stores;
using MVVMEssentials.Commands;

namespace mt.iwc.dataplusextension.wpf.Commands;

public class StopWindowsServiceCommand : CommandBase
{
    private readonly GlobalMessageStore _messageStore;
    private readonly WindowsServiceStore _windowsServiceStore;
    private bool _canExecute;

    public StopWindowsServiceCommand(GlobalMessageStore messageStore, WindowsServiceStore windowsServiceStore)
    {
        _messageStore = messageStore;
        _windowsServiceStore = windowsServiceStore;
        _windowsServiceStore.ServiceStateChanged += WindowsServiceStoreOnServiceStateChanged;
    }

    private void WindowsServiceStoreOnServiceStateChanged(DataPlusExtensionWindowsServiceState obj)
    {
        OnCanExecuteChanged();
    }

    public override bool CanExecute(object parameter)
    {

        _canExecute = _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Running || _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Paused;
        return _canExecute && base.CanExecute(parameter);
    }

    public override void Execute(object parameter)
    {
        try
        {
            _windowsServiceStore.StopWindowsService();
        }
        catch (Exception e)
        {
            _messageStore.UpdateMessage("Service konnte nicht gestoppt werden", GlobalMessageStore.MessageType.Error, e);

        }

    }
}

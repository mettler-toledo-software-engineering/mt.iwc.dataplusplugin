﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using mt.iwc.dataplusextension.application.Models;
//using mt.iwc.dataplusextension.application.Services;
//using mt.iwc.dataplusextension.wpf.Stores;
//using mt.iwc.dataplusextension.wpf.ViewModels;
//using MVVMEssentials.Commands;

//namespace mt.iwc.dataplusextension.wpf.Commands
//{
//    public class EstablishConnectionCommand : CommandBase
//    {
//        private readonly ScaleConnectionViewModel _scaleConnectionViewModel;
//        private readonly ScaleConnectionStore _scaleConnectionStore;
//        private readonly GlobalMessageStore _messageStore;
//        private bool _canExecute;

//        public EstablishConnectionCommand(ScaleConnectionViewModel scaleConnectionViewModel, ScaleConnectionStore scaleConnectionStore, GlobalMessageStore messageStore)
//        {
//            _scaleConnectionViewModel = scaleConnectionViewModel;
//            _scaleConnectionStore = scaleConnectionStore;
//            _messageStore = messageStore;
//            _scaleConnectionViewModel.PropertyChanged += ScaleConnectionViewModelOnPropertyChanged;
//        }


//        private void ScaleConnectionViewModelOnPropertyChanged(object? sender, PropertyChangedEventArgs e)
//        {
//            OnCanExecuteChanged();
//        }

//        public override bool CanExecute(object parameter)
//        {
//            _canExecute = _scaleConnectionViewModel.SelectedScaleComPort != null &&
//                          _scaleConnectionViewModel.SelectedDataPlusComPort != null &&
//                          !string.IsNullOrWhiteSpace(_scaleConnectionViewModel.ScaleName);
//            return base.CanExecute(parameter) && _canExecute;
//        }


//        public override void Execute(object parameter)
//        {
//            try
//            {
//                string? scaleComPort = _scaleConnectionViewModel.SelectedScaleComPort as string;
//                string? dataPlusComPort = _scaleConnectionViewModel.SelectedDataPlusComPort as string;

//                if (_scaleConnectionStore.RunningConnections.Any(c => c.Configuration.ScaleComPort == scaleComPort || c.Configuration.DataPlusComPort == dataPlusComPort))
//                {
//                    _messageStore.UpdateMessage("Verbindung wurde bereits aufgebaut", GlobalMessageStore.MessageType.Error);
//                    return;
//                }

//                var connection = _scaleConnectionStore.CreateConnection();
//                var configuration = new ConnectionConfiguration
//                {
//                    DataPlusComPort = dataPlusComPort,
//                    ScaleComPort = scaleComPort,
//                    ScaleName = _scaleConnectionViewModel.ScaleName,
//                    Scale = (ScaleType) _scaleConnectionViewModel.SelectedScaleType
//                };
//                var result = connection.ConnectScaleToDataPlus(configuration);

//                _scaleConnectionStore.AddRunningConnection(result);
//                _messageStore.UpdateMessage("Verbindung erfolgreich aufgebaut", GlobalMessageStore.MessageType.Success);

//            }
//            catch (Exception? e)
//            {
//                _messageStore.UpdateMessage("Verbindung fehlgeschlagen", GlobalMessageStore.MessageType.Error, e);
//                Console.WriteLine(e);

//            }
//            _scaleConnectionViewModel.HasBeenTested = true;
//        }
//    }
//}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.wpf.Stores;
using mt.iwc.dataplusextension.wpf.ViewModels;
using MVVMEssentials.Commands;

namespace mt.iwc.dataplusextension.wpf.Commands;

public class AddScaleConnectionViewCommand : CommandBase
{
    private readonly ScaleConnectionListingViewModel _scaleConnectionListingViewModel;
    private readonly ScaleConnectionStore _scaleConnectionStore;
    private readonly GlobalMessageStore _messageStore;
    private readonly WindowsServiceStore _windowsServiceStore;
    private bool _canExecute;

    public AddScaleConnectionViewCommand(ScaleConnectionListingViewModel scaleConnectionListingViewModel, ScaleConnectionStore scaleConnectionStore, GlobalMessageStore messageStore, WindowsServiceStore windowsServiceStore)
    {

        _scaleConnectionListingViewModel = scaleConnectionListingViewModel;
        _scaleConnectionStore = scaleConnectionStore;
        _messageStore = messageStore;
        _windowsServiceStore = windowsServiceStore;
        windowsServiceStore.ServiceStateChanged += WindowsServiceStoreOnServiceStateChanged;
    }

    private void WindowsServiceStoreOnServiceStateChanged(DataPlusExtensionWindowsServiceState obj)
    {
        OnCanExecuteChanged();
    }

    public override bool CanExecute(object parameter)
    {
        _canExecute = _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.NotFound ||
                      _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Paused ||
                      _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Stopped;
        return base.CanExecute(parameter) && _canExecute;
    }

    public override void Execute(object parameter)
    {
        ConnectionConfiguration connectionConfig = parameter as ConnectionConfiguration ?? new ConnectionConfiguration { ConnectionId = Guid.NewGuid()};
        ScaleConnectionViewModel connectionViewModel = new ScaleConnectionViewModel(_scaleConnectionStore, _messageStore, connectionConfig, _windowsServiceStore);
        _scaleConnectionListingViewModel.AddConnectionView(connectionViewModel);
        _scaleConnectionStore.AddScaleConnectionViewModel(connectionViewModel);
    }
    
}

﻿using System;
using System.ComponentModel;
using System.Linq;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.wpf.Stores;
using mt.iwc.dataplusextension.wpf.ViewModels;
using MVVMEssentials.Commands;

namespace mt.iwc.dataplusextension.wpf.Commands;

public class SaveScaleConnectionsCommand : CommandBase
{
    private readonly ScaleConnectionViewModel _scaleConnectionViewModel;
    private readonly ScaleConnectionStore _scaleConnectionStore;
    private readonly GlobalMessageStore _messageStore;
    private readonly WindowsServiceStore _windowsServiceStore;
    private bool _canExecute;

    public SaveScaleConnectionsCommand(ScaleConnectionStore scaleConnectionStore, GlobalMessageStore messageStore, WindowsServiceStore windowsServiceStore)
    {
        
        _scaleConnectionStore = scaleConnectionStore;
        _messageStore = messageStore;
        _windowsServiceStore = windowsServiceStore;
        windowsServiceStore.ServiceStateChanged += WindowsServiceStoreOnServiceStateChanged;
        scaleConnectionStore.ScaleConnectionViewModelChanged += ScaleConnectionStoreOnScaleConnectionViewModelChanged;
        scaleConnectionStore.ConnectionDeleted += ScaleConnectionStoreOnConnectionDeleted;
    }

    private void ScaleConnectionStoreOnConnectionDeleted(ScaleConnectionViewModel? obj)
    {
        OnCanExecuteChanged();
    }

    private void ScaleConnectionStoreOnScaleConnectionViewModelChanged()
    {
        OnCanExecuteChanged();
    }

    private void WindowsServiceStoreOnServiceStateChanged(DataPlusExtensionWindowsServiceState obj)
    {
        OnCanExecuteChanged();
    }

    public override bool CanExecute(object parameter)
    {
        _canExecute = (_scaleConnectionStore.ScaleConnectionViewModels.Any(x => x.HasBeenTested == false) == false)&& 
                      (_windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.NotFound ||
                      _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Paused ||
                      _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Stopped);

        return base.CanExecute(parameter) && _canExecute;
    }

    public override void Execute(object parameter)
    {
        try
        {
            _scaleConnectionStore.SaveScaleConnections();
            _messageStore.UpdateMessage("Verbindungen wurden gespeichert", GlobalMessageStore.MessageType.Info);
        }
        catch (Exception e)
        {
            _messageStore.UpdateMessage("Verbindungen konnten nicht gespeichert werden", GlobalMessageStore.MessageType.Error, e);
        }
        

        
    }
}

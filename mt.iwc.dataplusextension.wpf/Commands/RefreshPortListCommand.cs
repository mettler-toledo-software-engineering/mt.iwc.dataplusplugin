﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.wpf.ViewModels;
using MVVMEssentials.Commands;
using MVVMEssentials.ViewModels;

namespace mt.iwc.dataplusextension.wpf.Commands;

public class RefreshPortListCommand : CommandBase
{
    private readonly MainWindowViewModel _mainViewModel;

    public RefreshPortListCommand(MainWindowViewModel mainViewModel)
    {
        _mainViewModel = mainViewModel;
    }
    public override void Execute(object parameter)
    {
        _mainViewModel.PortNames = new List<string>(SerialPort.GetPortNames());
    }
}

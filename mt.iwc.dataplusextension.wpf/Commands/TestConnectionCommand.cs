﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Services.Communication;
using mt.iwc.dataplusextension.wpf.Stores;
using mt.iwc.dataplusextension.wpf.ViewModels;
using MVVMEssentials.Commands;

namespace mt.iwc.dataplusextension.wpf.Commands;

public class TestConnectionCommand : CommandBase, IDisposable
{
    private readonly ScaleConnectionViewModel _scaleConnectionViewModel;
    private readonly ScaleConnectionStore _scaleConnectionStore;
    private readonly GlobalMessageStore _messageStore;
    private readonly WindowsServiceStore _windowsServiceStore;
    private bool _canExecute;

    public TestConnectionCommand(ScaleConnectionViewModel scaleConnectionViewModel, ScaleConnectionStore scaleConnectionStore, GlobalMessageStore messageStore, WindowsServiceStore windowsServiceStore)
    {
        _scaleConnectionViewModel = scaleConnectionViewModel;
        _scaleConnectionStore = scaleConnectionStore;
        _messageStore = messageStore;
        _windowsServiceStore = windowsServiceStore;
        _windowsServiceStore.ServiceStateChanged += WindowsServiceStoreOnServiceStateChanged;
        _scaleConnectionViewModel.PropertyChanged += ScaleConnectionViewModelOnPropertyChanged;
        
    }

    private void WindowsServiceStoreOnServiceStateChanged(DataPlusExtensionWindowsServiceState obj)
    {
        OnCanExecuteChanged();
    }


    private void ScaleConnectionViewModelOnPropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        OnCanExecuteChanged();
    }

    public override bool CanExecute(object parameter)
    {
        _canExecute = _scaleConnectionViewModel.SelectedScaleComPort != null &&
                      _scaleConnectionViewModel.SelectedDataPlusComPort != null &&
                      !string.IsNullOrWhiteSpace(_scaleConnectionViewModel.ScaleName) && 
                      (_windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.NotFound ||
                       _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Paused ||
                       _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Stopped
                      );
        return base.CanExecute(parameter) && _canExecute;
    }

    public override void Execute(object parameter)
    {
        
        try
        {
            string? scaleComPort = _scaleConnectionViewModel.SelectedScaleComPort as string;
            string? dataPlusComPort = _scaleConnectionViewModel.SelectedDataPlusComPort as string;

            var configuration = new ConnectionConfiguration
            {
                DataPlusComPort = dataPlusComPort,
                ScaleComPort = scaleComPort,
                ScaleName = _scaleConnectionViewModel.ScaleName,
                Scale = (ScaleType)_scaleConnectionViewModel.SelectedScaleType,
                ConnectionId = _scaleConnectionViewModel.ConnectionId
            };

            if (_scaleConnectionStore.ScaleConnections != null && _scaleConnectionStore.ScaleConnections.Any(c => c.DataPlusComPort == configuration.DataPlusComPort && c.ConnectionId != configuration.ConnectionId))
            {
                _messageStore.UpdateMessage("DataPlus COM Port bereits in Verwendung", GlobalMessageStore.MessageType.Error);
                return;
            }
            if (_scaleConnectionStore.ScaleConnections != null && _scaleConnectionStore.ScaleConnections.Any(c => c.ScaleComPort == configuration.ScaleComPort && c.ConnectionId != configuration.ConnectionId))
            {
                _messageStore.UpdateMessage("Waagen COM Port bereits in Verwendung", GlobalMessageStore.MessageType.Error);
                return;
            }

            if (_scaleConnectionStore.ScaleConnections != null && _scaleConnectionStore.ScaleConnections.Any(c => c.ScaleName == configuration.ScaleName && c.ConnectionId != configuration.ConnectionId))
            {
                _messageStore.UpdateMessage("Waagen Bezeichnung bereits in Verwendung", GlobalMessageStore.MessageType.Error);
                return;
            }

            using var connection = _scaleConnectionStore.CreateConnection();
            var result = connection.ConnectScaleToDataPlus(configuration);
            _messageStore.UpdateMessage("Verbindung erfolgreich",GlobalMessageStore.MessageType.Success);
            _scaleConnectionViewModel.ConnectionState = ConnectionState.TestedConnection;

            _scaleConnectionStore.AddScaleConnection(configuration);
        }
        catch (Exception e)
        {
            _messageStore.UpdateMessage("Verbindung fehlgeschlagen",GlobalMessageStore.MessageType.Error, e);
            _scaleConnectionViewModel.ConnectionState = ConnectionState.Disconnected;

        }
        _scaleConnectionViewModel.HasBeenTested = true;
        
    }

    public void Dispose()
    {
        _scaleConnectionViewModel.PropertyChanged -= ScaleConnectionViewModelOnPropertyChanged;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using MVVMEssentials.Commands;

namespace mt.iwc.dataplusextension.wpf.Commands;


public class InstallWindowsService : CommandBase
{
    
    public override void Execute(object parameter)
    {
        Process p = new Process();
        // Redirect the output stream of the child process.
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.CreateNoWindow = true;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.FileName = "installService.bat";
        p.StartInfo.Verb = "runas"; //start process as administrator
      
        p.Start();
        // Do not wait for the child process to exit before
        // reading to the end of its redirected stream.
        // p.WaitForExit();
        // Read the output stream first and then wait.
        string output = p.StandardOutput.ReadToEnd();
        p.OutputDataReceived += POnOutputDataReceived;
        p.ErrorDataReceived += POnErrorDataReceived;
        p.WaitForExit();
    }

    private void POnErrorDataReceived(object sender, DataReceivedEventArgs e)
    {
        throw new NotImplementedException();
    }

    private void POnOutputDataReceived(object sender, DataReceivedEventArgs e)
    {
        throw new NotImplementedException();
    }
}

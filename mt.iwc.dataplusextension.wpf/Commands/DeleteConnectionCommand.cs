﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.wpf.Stores;
using mt.iwc.dataplusextension.wpf.ViewModels;
using MVVMEssentials.Commands;

namespace mt.iwc.dataplusextension.wpf.Commands;

public class DeleteConnectionCommand :CommandBase
{
    private readonly ScaleConnectionStore _scaleConnectionStore;
    private readonly GlobalMessageStore _messageStore;
    private readonly ScaleConnectionViewModel _scaleConnectionViewModel;
    private readonly WindowsServiceStore _windowsServiceStore;
    private bool _canExecute;

    public DeleteConnectionCommand(ScaleConnectionStore scaleConnectionStore, GlobalMessageStore messageStore, ScaleConnectionViewModel scaleConnectionViewModel, WindowsServiceStore windowsServiceStore)
    {
        _scaleConnectionStore = scaleConnectionStore;
        _messageStore = messageStore;
        _scaleConnectionViewModel = scaleConnectionViewModel;
        _windowsServiceStore = windowsServiceStore;
        windowsServiceStore.ServiceStateChanged += WindowsServiceStoreOnServiceStateChanged;
    }

    private void WindowsServiceStoreOnServiceStateChanged(DataPlusExtensionWindowsServiceState obj)
    {
        OnCanExecuteChanged();
    }

    public override bool CanExecute(object parameter)
    {
        _canExecute = _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.NotFound ||
                      _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Paused ||
                      _windowsServiceStore.DataPlusExtensionWindowsService.ServiceState == DataPlusExtensionWindowsServiceState.Stopped;
            
        return _canExecute && base.CanExecute(parameter);
    }

    public override void Execute(object parameter)
    {
        try
        {
            Guid connectionId = _scaleConnectionViewModel.ConnectionId;

            _scaleConnectionStore.DeleteConnection(connectionId);
        }
        catch (Exception e)
        {
            _messageStore.UpdateMessage("Verbindung konnte nicht gelöscht werden.",GlobalMessageStore.MessageType.Error,e);
        }
        
    }
}

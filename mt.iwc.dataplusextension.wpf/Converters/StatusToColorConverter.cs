﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using mt.iwc.dataplusextension.application.Services.Communication;

namespace mt.iwc.dataplusextension.wpf.Converters;

public class StatusToColorConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is ConnectionState)
        {
            ConnectionState state = (ConnectionState) value;
            Color background = Colors.AliceBlue;

            switch (state)
            {
                case ConnectionState.TestedConnection:
                    background = Colors.Aquamarine;
                    break;
                case ConnectionState.Connected:
                    background = Colors.GreenYellow;
                    break;
                case ConnectionState.Disconnected:
                    background = Colors.Firebrick;
                    break;
                case ConnectionState.NewConnection:
                    background = Colors.AliceBlue;
                    break;
            }

            return background;
        }

        return Binding.DoNothing;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}

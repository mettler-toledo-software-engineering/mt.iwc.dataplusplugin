﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using mt.iwc.dataplusextension.application.Services.Communication;

namespace mt.iwc.dataplusextension.wpf.Converters;

public class StatusToTextConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is ConnectionState)
        {
            ConnectionState state = (ConnectionState)value;
           string text = string.Empty;

            switch (state)
            {
                case ConnectionState.Connected:
                    text = "Verbunden";
                    break;                
                case ConnectionState.TestedConnection:
                    text = "Verbindung getestet";
                    break;
                case ConnectionState.Disconnected:
                    text = "Verbindung getrennt";
                    break;
                case ConnectionState.NewConnection:
                    text = "Neue Verbindung";
                    break;
            }

            return text;
        }

        return Binding.DoNothing;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.wpf.Converters;

public class WindowsServiceStateToTextConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is DataPlusExtensionWindowsServiceState)
        {
            DataPlusExtensionWindowsServiceState state = (DataPlusExtensionWindowsServiceState)value;
            string text = string.Empty;

            switch (state)
            {
                case DataPlusExtensionWindowsServiceState.Paused:
                    text = "Pausiert";
                    break;
                case DataPlusExtensionWindowsServiceState.Starting:
                    text = "wird gestartet";
                    break;
                case DataPlusExtensionWindowsServiceState.Stopping:
                    text = "wird gestoppt";
                    break;
                case DataPlusExtensionWindowsServiceState.Stopped:
                    text = "gestoppt";
                    break;
                case DataPlusExtensionWindowsServiceState.NotFound:
                    text = "nicht gefunden / installiert";
                    break;
                case DataPlusExtensionWindowsServiceState.Running:
                    text = "läuft";
                    break;
            }

            return text;
        }

        return Binding.DoNothing;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.wpf.Converters;

public class WindowsServiceStateToColorConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is DataPlusExtensionWindowsServiceState)
        {
            DataPlusExtensionWindowsServiceState state = (DataPlusExtensionWindowsServiceState)value;
            SolidColorBrush background = new SolidColorBrush(Colors.AliceBlue);

            switch (state)
            {
                case DataPlusExtensionWindowsServiceState.Paused:
                    background = new SolidColorBrush(Colors.Yellow);
                    break;
                case DataPlusExtensionWindowsServiceState.Starting:
                    background = new SolidColorBrush(Colors.AliceBlue);
                    break;
                case DataPlusExtensionWindowsServiceState.Stopping:
                    background = new SolidColorBrush(Colors.AliceBlue);
                    break;
                case DataPlusExtensionWindowsServiceState.Stopped:
                    background = new SolidColorBrush(Colors.Yellow);
                    break;
                case DataPlusExtensionWindowsServiceState.NotFound:
                    background = new SolidColorBrush(Colors.Firebrick);
                    break;
                case DataPlusExtensionWindowsServiceState.Running:
                    background = new SolidColorBrush(Colors.GreenYellow);
                    break;
            }

            return background;
        }

        return Binding.DoNothing;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}

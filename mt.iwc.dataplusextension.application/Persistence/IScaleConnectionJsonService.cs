﻿using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.application.Persistence
{
    public interface IScaleConnectionJsonService
    {
        List<ConnectionConfiguration>? LoadScaleConnections();
        void SaveScaleConnections(List<ConnectionConfiguration>? connections);
    }
}
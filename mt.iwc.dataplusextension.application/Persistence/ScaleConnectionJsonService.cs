﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;



namespace mt.iwc.dataplusextension.application.Persistence
{
    public class ScaleConnectionJsonService : IScaleConnectionJsonService
    {
        string _fileName = @"c:\DataPlusExtension\Data\ScaleConnections.json";
        public List<ConnectionConfiguration>? LoadScaleConnections()
        {

            string jsonString = File.ReadAllText(_fileName);
            if (string.IsNullOrWhiteSpace(jsonString))
            {
                return new List<ConnectionConfiguration>();
            }
            List<ConnectionConfiguration>? scaleconnections =  JsonSerializer.Deserialize<List<ConnectionConfiguration>>(jsonString);

            return scaleconnections;
        }

        public void SaveScaleConnections(List<ConnectionConfiguration>? connections)
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
            string jsonString = JsonSerializer.Serialize(connections, options);
            File.WriteAllText(_fileName, jsonString);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using mt.iwc.dataplusextension.application.Models;


namespace mt.iwc.dataplusextension.application.Services.Communication;

public class DataPlusExtensionWindowsServiceService : IDataPlusExtensionWindowsServiceService
{
    private readonly DataPlusExtensionWindowsService _windowsService = new();
    public DataPlusExtensionWindowsServiceState GetDataPlusExtensionWindowsServiceState()
    {
        ServiceController sc = new ServiceController(DataPlusExtensionWindowsService.SerivceName);

        DataPlusExtensionWindowsServiceState state = new DataPlusExtensionWindowsServiceState();
        try
        {
            switch (sc.Status)
            {
                case ServiceControllerStatus.Running:
                    state = DataPlusExtensionWindowsServiceState.Running;
                    break;
                case ServiceControllerStatus.Stopped:
                    state = DataPlusExtensionWindowsServiceState.Stopped;
                    break;
                case ServiceControllerStatus.Paused:
                    state = DataPlusExtensionWindowsServiceState.Paused;
                    break;
                case ServiceControllerStatus.StopPending:
                    state = DataPlusExtensionWindowsServiceState.Stopping;
                    break;
                case ServiceControllerStatus.StartPending:
                    state = DataPlusExtensionWindowsServiceState.Starting;
                    break;
                default:
                    break;
            }
        }
        catch (InvalidOperationException)
        {
            state = DataPlusExtensionWindowsServiceState.NotFound;
        }
        return state;
    }

    public DataPlusExtensionWindowsService GetDataPlusExtensionWindowsService()
    {
        _windowsService.ServiceState = GetDataPlusExtensionWindowsServiceState();

        return _windowsService;
    }

    public void StartDataPlusExtensionWindowsService()
    {
        ServiceController sc = new ServiceController(DataPlusExtensionWindowsService.SerivceName);
        sc.Start();
    }

    public void StopDataPlusExtensionWindowsService()
    {
        ServiceController sc = new ServiceController(DataPlusExtensionWindowsService.SerivceName);
        sc.Stop();
    }
}

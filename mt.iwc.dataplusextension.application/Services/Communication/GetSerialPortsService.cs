﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.iwc.dataplusextension.application.Services.Communication
{
    public class GetSerialPortsService : IGetSerialPortsService
    {
        public IEnumerable<string> GetSerialPortNames()
        {
            return SerialPort.GetPortNames();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.iwc.dataplusextension.application.Services.Communication;

public class SerialConnectionService : ISerialConnectionService
{
    private SerialPort? _serialPort;

    public event Action<string>? DataReceived;
    private string? _data;
    private string _eol = "\r\n";

    private ConnectionResult _connectionResult = ConnectionResult.Failure;
    // DataPlus Port 4302 
    // service user nötig damit die applikation als dienst auf netzwerk zugreifen darf

    public ConnectionResult ConnectToSerialPort(string? comPort)
    {
        if (comPort != null) _serialPort = new SerialPort(comPort, 9600, Parity.None, 8, StopBits.One);

        if (_serialPort != null && !_serialPort.IsOpen)
        {
            _serialPort.Open();
        }

        if (_serialPort != null)
        {
            _serialPort.DataReceived += SerialPortOnDataReceived;
        }
        _connectionResult = ConnectionResult.Success;

        return _connectionResult;
    }


    private void SerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
    {
        //todo muss überarbeitet werden, falls der crlf mitten in den Daten ist

        _data += _serialPort?.ReadExisting();

        if (_data.EndsWith(_eol))
        {

            var results = _data.Split(_eol);
            foreach (var result in results)
            {
                if (!string.IsNullOrWhiteSpace(result))
                {
                    DataReceived?.Invoke($"{result}{_eol}");
                }
            }
            
            _data = string.Empty;
        }
        //read scale data
        //parse scale data and reformat for data+
        //write data to virtual data+ port
    }

    public void SendData(string data)
    {
        _serialPort?.Write(data);
    }

    public void Dispose()
    {
        _serialPort?.Close();
        _serialPort?.Dispose();
    }

    public ConnectionState CheckConnectionState()
    {
        return _serialPort != null && _serialPort.IsOpen ? ConnectionState.Connected : ConnectionState.Disconnected;
    }
}

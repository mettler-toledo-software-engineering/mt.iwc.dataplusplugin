﻿using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.application.Services.Communication
{
    public interface IDataPlusExtensionWindowsServiceService
    {
        DataPlusExtensionWindowsServiceState GetDataPlusExtensionWindowsServiceState();
        DataPlusExtensionWindowsService GetDataPlusExtensionWindowsService();
        void StartDataPlusExtensionWindowsService();
        void StopDataPlusExtensionWindowsService();
    }
}
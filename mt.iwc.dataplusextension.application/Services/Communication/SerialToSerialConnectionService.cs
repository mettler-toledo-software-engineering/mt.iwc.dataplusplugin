﻿
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Services.Translation;
using Serilog;

namespace mt.iwc.dataplusextension.application.Services.Communication;

public class SerialToSerialConnectionService : ISerialToSerialConnectionService
{

    private readonly ILogger _logger;
    private ISerialConnectionService _scaleConnection;
    private ISerialConnectionService _dataplusConnection;
    private readonly IDataTranslationService _dataTranslation;

    public string? ScaleComPort { get; private set; }
    public string? DataPlusComPort { get; private set; }
    public ScaleType Scale { get; private set; }
    public Guid ConnectionId { get; private set; }
    public ConnectionState ConnectionState { get; set; }

    public SerialToSerialConnectionService(ILogger logger, ISerialConnectionService scaleConnection, ISerialConnectionService dataplusConnection, IDataTranslationService dataTranslation)
    {
        _logger = logger;
        _scaleConnection = scaleConnection;
        _dataplusConnection = dataplusConnection;
        _dataTranslation = dataTranslation;
    }
    public ScaleToDataPlusConnection ConnectScaleToDataPlus(ConnectionConfiguration connectionConfiguration)
    {
        ScaleComPort = connectionConfiguration.ScaleComPort;
        DataPlusComPort = connectionConfiguration.DataPlusComPort;
        Scale = connectionConfiguration.Scale;
        ConnectionId = connectionConfiguration.ConnectionId;

        _scaleConnection = new SerialConnectionService();
        _dataplusConnection = new SerialConnectionService();

        var serialResult = _scaleConnection.ConnectToSerialPort(ScaleComPort);
        var tcpResult = _dataplusConnection.ConnectToSerialPort(DataPlusComPort);

        if (serialResult == ConnectionResult.Success && tcpResult == ConnectionResult.Success)
        {
            ConnectionState = ConnectionState.Connected;
        }

        _scaleConnection.DataReceived += ScaleConnectionOnDataReceived;
        _dataplusConnection.DataReceived += DataplusConnectionOnDataReceived;

        return new ScaleToDataPlusConnection
        {
            ConnectionState = ConnectionState,
            Configuration = connectionConfiguration,
            SerialConnection = this
        };
    }

    private void DataplusConnectionOnDataReceived(string dataPlusData)
    {

        if (_dataTranslation.KnownDataPlusRequests.Contains(dataPlusData))
        {
            string response = _dataTranslation.GetDefaultResponseData(dataPlusData);
            _dataplusConnection.SendData(response);
            _logger.Information($"Default -> Dataplus: {response}");
            return;
        }

        string translatedData = _dataTranslation.TranslateData(dataPlusData, Scale, DataDirection.ToScale);
        _scaleConnection.SendData(translatedData);
        _logger.Information($"DataPlus -> Scale: {translatedData}");
    }

    private void ScaleConnectionOnDataReceived(string scaleData)
    {
        string translatedData = _dataTranslation.TranslateData(scaleData, Scale, DataDirection.ToDataPlus);
        _dataplusConnection.SendData(translatedData);
        _logger.Information($"Scale -> Dataplus: {translatedData}");
    }


    public void Dispose()
    {
        _scaleConnection.Dispose();
        _scaleConnection.DataReceived -= ScaleConnectionOnDataReceived;
        _dataplusConnection.Dispose();
        _dataplusConnection.DataReceived -= DataplusConnectionOnDataReceived;

    }

    public ConnectionState CheckConnectionState()
    {
        ConnectionState result = ConnectionState.Disconnected;

        var dataPlusConnectionState = _dataplusConnection.CheckConnectionState();
        var scaleConnectionState = _scaleConnection.CheckConnectionState();

        if (dataPlusConnectionState == ConnectionState.Connected && scaleConnectionState == ConnectionState.Connected)
        {
            return ConnectionState.Connected;
        }
        else
        {
            return ConnectionState.Disconnected;
        }
    }

}

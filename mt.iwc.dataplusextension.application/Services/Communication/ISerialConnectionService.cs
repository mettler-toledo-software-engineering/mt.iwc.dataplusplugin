﻿
namespace mt.iwc.dataplusextension.application.Services.Communication;


public enum ConnectionResult
{
    Success,
    Failure
}
public interface ISerialConnectionService : IDisposable
{
    event Action<string>? DataReceived;
    ConnectionResult ConnectToSerialPort(string? comPort);
    ConnectionState CheckConnectionState();
    void SendData(string data);
    new void Dispose();

}

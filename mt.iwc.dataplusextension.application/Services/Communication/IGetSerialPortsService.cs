﻿
namespace mt.iwc.dataplusextension.application.Services.Communication
{
    public interface IGetSerialPortsService
    {
        IEnumerable<string> GetSerialPortNames();
    }
}
﻿using System.IO.Ports;
using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.application.Services.Communication;

public enum ConnectionState
{
    Connected,
    Disconnected,
    NewConnection,
    TestedConnection
}

public interface ISerialToSerialConnectionService : IDisposable
{
    ScaleToDataPlusConnection ConnectScaleToDataPlus(ConnectionConfiguration connectionConfiguration);

    ConnectionState CheckConnectionState();
    new void Dispose();
}

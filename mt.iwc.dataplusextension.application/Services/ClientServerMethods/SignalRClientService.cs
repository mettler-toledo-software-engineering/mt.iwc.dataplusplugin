﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Services.Communication;

namespace mt.iwc.dataplusextension.application.Services.ClientServerMethods;

public class SignalRClientService : IClientMethods, ISignalRClientService
{
    private readonly HubConnection _connection;
    public event Action<string> ServerStatusReceived;
    public event Action<ScaleToDataPlusConnection> ActiveScaleConnectionReceived;
    public event Action<ConnectionState, Guid> ConnectionStateReceived;
    
    private int _retryCount = 0;
    public SignalRClientService(HubConnection connection)
    {
        _connection = connection;
        _connection.On<string>(nameof(GetServerStatus), status => GetServerStatus(status));
        _connection.On<ScaleToDataPlusConnection>(nameof(GetEstablishedConnection), (connections) => GetEstablishedConnection(connections));
        _connection.On<ConnectionState,Guid>(nameof(GetConnectionState), (connectionstate, id) => GetConnectionState(connectionstate, id));
    }

    public async Task Connect()
    {
        await _connection.StartAsync();
    }
    public async Task ConnectWithRetry()
    {
        
        try
        {
            await _connection.StartAsync();
            _retryCount = 0;
        }
        catch (Exception)
        {
            if (_retryCount > 5)
            {
                throw;
            }
            Thread.Sleep(5000);
            _retryCount++;
            await ConnectWithRetry();

        }
        
    }

    public Task GetServerStatus(string status)
    {
        ServerStatusReceived?.Invoke($"{DateTime.Now:dd.MM.yyyy HH:mm:ss} {status}");
        return Task.CompletedTask;
    }

    public async Task Disconnect()
    {
        await _connection.StopAsync();
    }

    public Task GetEstablishedConnection(ScaleToDataPlusConnection connection)
    {
        ActiveScaleConnectionReceived?.Invoke(connection);
        return Task.CompletedTask;
    }

    public Task GetConnectionState(ConnectionState state, Guid connectionId)
    {
        ConnectionStateReceived?.Invoke(state,connectionId);
        return Task.CompletedTask;
    }
}

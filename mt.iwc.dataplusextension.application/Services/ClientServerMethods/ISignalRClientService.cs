﻿using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Services.Communication;

namespace mt.iwc.dataplusextension.application.Services.ClientServerMethods
{
    public interface ISignalRClientService
    {
        event Action<ConnectionState, Guid> ConnectionStateReceived;
        event Action<string> ServerStatusReceived;

        Task Connect();

        Task Disconnect();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Services.Communication;

namespace mt.iwc.dataplusextension.application.Services.ClientServerMethods;

public interface IClientMethods
{
    Task GetServerStatus(string status);
    Task GetEstablishedConnection(ScaleToDataPlusConnection connection);
    Task GetConnectionState(ConnectionState state, Guid connectionId);
}

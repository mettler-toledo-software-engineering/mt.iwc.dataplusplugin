﻿using mt.iwc.dataplusextension.application.Services.Communication;

namespace mt.iwc.dataplusextension.application.Services.Factories;

public interface ISerialToSerialConnectionFactory
{
    ISerialToSerialConnectionService CreateConnectionService();
}
﻿using mt.iwc.dataplusextension.application.Services.Communication;
using mt.iwc.dataplusextension.application.Services.Translation;
using Serilog;

namespace mt.iwc.dataplusextension.application.Services.Factories;

public class SerialToSerialConnectionFactory : ISerialToSerialConnectionFactory
{
    private readonly ILogger _logger;
    private ISerialConnectionService _scaleConnection;
    private ISerialConnectionService _dataplusConnection;
    private readonly IDataTranslationService _dataTranslationService;

    public SerialToSerialConnectionFactory(ILogger logger)
    {
        _logger = logger;
        _dataTranslationService = new DataTranslationService();
    }

    public ISerialToSerialConnectionService CreateConnectionService()
    {
        _scaleConnection = new SerialConnectionService();
        _dataplusConnection = new SerialConnectionService();

        return new SerialToSerialConnectionService(_logger, _scaleConnection, _dataplusConnection, _dataTranslationService);
    }
}

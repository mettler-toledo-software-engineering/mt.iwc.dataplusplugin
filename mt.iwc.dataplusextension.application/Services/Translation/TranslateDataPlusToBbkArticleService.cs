﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.application.Services.Translation
{
    internal class TranslateDataPlusToBbkArticleService : ITranslateDataPlusToBbkArticleService
    {
        private const string Eol = "\r\n";

        public string CreateBbkArticleResponse(BbkArticle bbkArticle)
        {

            string output = $"{CreateArticleLine1()}{CreateArticleLine2(bbkArticle)}{CreateArticleLine3(bbkArticle)}{CreateArticleLine4(bbkArticle)}{CreateArticleLine5()}";
            return output;
        }

        private string CreateArticleLine1()
        {
            string output = "CMD6 A" + Eol;
            return output;
        }
        private string CreateArticleLine2(BbkArticle bbkArticle)
        {
            //7E "IWA00497" "Number" "ID1" "ID2" 0.071000 8.
            string output = $"{bbkArticle.Name} {bbkArticle.Number} {bbkArticle.Id1} {bbkArticle.Id2} {bbkArticle.AveragePieceWeight} 0.{Eol}";
            string checksum = CreateCheckSum(output);
            output = $"{checksum} {output}";
            return output;
        }
        private string CreateArticleLine3(BbkArticle bbkArticle)
        {
            //94 500 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
            string output = $"{bbkArticle.ReferenceSize} 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.{Eol}";
            string checksum = CreateCheckSum(output);
            output = $"{checksum} {output}";
            return output;
        }
        private string CreateArticleLine4(BbkArticle bbkArticle)
        {
            //64 "0" 0 0 0 0 "" "22.10.2019" "00.00.00 00:00:00" 0

            string output = $"\"0\" 0 0 0 0 \"\" {bbkArticle.Text2} \"00.00.00 00:00:00\" 0{Eol}";
            string checksum = CreateCheckSum(output);
            output = $"{checksum} {output}";
            return output;
        }

        private string CreateArticleLine5()
        {
            // EOF "14.12.2021" "11:24:34"

            string output = $"EOF \"{DateTime.Now:dd.MM.yyyy}\" \"{DateTime.Now:HH:mm:ss}\"{Eol}";
            return output;
        }
        private string CreateCheckSum(string data)
        {
            int checksum = 0;

            foreach (char c in data)
            {
                checksum += Convert.ToInt32(c);
            }

            checksum %= 256;

            return checksum.ToString("X");
        }
    }
}

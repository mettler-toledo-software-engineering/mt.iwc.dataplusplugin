﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.application.Services.Translation;

public class DataTranslationService : IDataTranslationService
{
    private const string Eol = "\r\n";
    private const string DataPlusRequestI2 = "I2" + Eol;
    private const string DataPlusRequestI3 = "I3" + Eol;
    private const string DataPlusRequestPass = "PASS 07813" + Eol;
    private const string DataPlusRequestInfo = "INFO" + Eol;
    private const string DataPlusRequestSop = "SOP" + Eol;
    
    private const string DataPlusRequestQuit = "QUIT" + Eol;

    private const string BbkResponseToI2 = "I2 A \"ICS685-MF 6.1090 kg\"" + Eol;
    private const string BbkResponseToI3 = "I3 A \"E6-DS-01.10.12-MF-8\"" + Eol;
    private const string BbkResponseToPass = "ACK    12 Access OK." + Eol;
    private const string BbkResponseToInfo = "TERMINAL NAME : ICS685  S/W VERSION : 01.10.12" + Eol;
    private const string BbkResponseToSop = "SOP A STANDARD" + Eol;
    
    private const string BbkResponseToQuit = "QUIT 31" + Eol;



    public List<string> KnownDataPlusRequests => new() { DataPlusRequestI2, DataPlusRequestI3, DataPlusRequestPass, DataPlusRequestInfo, DataPlusRequestSop, DataPlusRequestQuit };


    public string GetDefaultResponseData(string dataplusRequest)
    {
        string output = string.Empty;

        output = dataplusRequest switch
        {
            DataPlusRequestI2 => BbkResponseToI2,
            DataPlusRequestI3 => BbkResponseToI3,
            DataPlusRequestPass => BbkResponseToPass,
            DataPlusRequestInfo => BbkResponseToInfo,
            DataPlusRequestSop => BbkResponseToSop,
            DataPlusRequestQuit => BbkResponseToQuit,
            _ => output,
        };

        return output;
    }
    public string TranslateData(string data, ScaleType scaleType, DataDirection dataDirection)
    {
        return scaleType switch
        {
            ScaleType.BBK => dataDirection switch
            {
                DataDirection.ToScale => data.TranslateDataPlusToBbkData(),
                DataDirection.ToDataPlus => data.TranslateBbkToDataPlusData(),
                _ => throw new ArgumentOutOfRangeException(nameof(dataDirection), dataDirection, null)
            },
            ScaleType.ICS => data,
            _ => throw new ArgumentOutOfRangeException(nameof(scaleType), scaleType, null)
        };
    }
}

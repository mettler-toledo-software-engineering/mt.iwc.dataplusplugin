﻿using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.application.Services.Translation
{
    internal interface ITranslateDataPlusToBbkArticleService
    {
        string CreateBbkArticleResponse(BbkArticle bbkArticle);
    }
}
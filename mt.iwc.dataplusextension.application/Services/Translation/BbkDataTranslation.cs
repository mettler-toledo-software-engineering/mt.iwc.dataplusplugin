﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.application.Services.Translation;

internal static class BbkDataTranslation
{

    private const string Eol = "\r\n";

    private const string DataPlusRequestReadSnr = "READ TERMINAL.SERIALNUMBER" + Eol;
    private const string BbkRequestReadSnr = "I4" + Eol;

    private const string BBkResponseI4A = "I4 A ";

    private const string BBkRequestCmd6 = "CMD6 ";

    private const string DataPlusResponseArticleEmpty = "DBC A EMPTY"+Eol;
    private const string DataPlusResponseArticle = "DBC A";

    private const string SmartManagerResponseOk = "CMD6 A" + Eol;
    private const string SmartManagerResponseFail = "BREAK_N" + Eol;



    internal static string TranslateDataPlusToBbkData(this string data)
    {
        string output = string.Empty;

        if (data == DataPlusRequestReadSnr)
        {
            output = BbkRequestReadSnr;
        }

        if (data == DataPlusResponseArticleEmpty)
        {
            output = SmartManagerResponseFail;
        }
        else if (data.StartsWith(DataPlusResponseArticle))
        {
            output = TranslateDbcAToBbk(data);
        }

        return output;
    }

    internal static string TranslateBbkToDataPlusData(this string data)
    {
        string output = string.Empty;

        if (data.StartsWith(BBkResponseI4A))
        {
            output = TranslateI4ToDataPlus(data);
        }

        if (data.StartsWith(BBkRequestCmd6))
        {
            output = TranslateCmd6ToDataPlus(data);
        }

        return output;
    }



    private static string TranslateI4ToDataPlus(string data)
    {
        //I4 A "3097854"
        string snr = data.Replace($"{BBkResponseI4A}", "");
        return $"ACK    TERMINAL.SERIALNUMBER {snr}{Eol}";
    }

    private static string TranslateCmd6ToDataPlus(string data)
    {
        string article = data.Replace(BBkRequestCmd6, "");
        return $"DBC GET{article}";
    }

    private static string TranslateDbcAToBbk(string data)
    {
        //DBC A "10" "Test" "info1" "info2" "" "0.0" "g" "PieceCounting" "PCS" "2" "2" "g" "0.0" "%" "10" "PCS" "Absolute" "0.0" "g" "0.0" "g" "CheckWeighing" "Absolute" "0.0" "PCS" "0.0" "PCS" "0.0" "PCS" "LotN" "0.0" "PCS" "0.0" "g" "0.0" "g" "0.0" "PCS" "0.0" 96
        var dataplusarticle = new DataPlusResponseArticle(data);
        var bbkartikel = new BbkArticle(dataplusarticle);
        var artikeltranslation = new TranslateDataPlusToBbkArticleService();
        string output = artikeltranslation.CreateBbkArticleResponse(bbkartikel);
        return output;
    }
}

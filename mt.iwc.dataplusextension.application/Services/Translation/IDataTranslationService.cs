﻿using mt.iwc.dataplusextension.application.Models;

namespace mt.iwc.dataplusextension.application.Services.Translation;

public enum DataDirection
{
    ToScale,
    ToDataPlus,
}
public interface IDataTranslationService
{
    string TranslateData(string data, ScaleType scaleType, DataDirection direction);
    List<string> KnownDataPlusRequests { get; }
    string GetDefaultResponseData(string dataplusRequest);
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.iwc.dataplusextension.application.Models;

internal class BbkArticle
{

    public string Name { get; set; }
    public string Number { get; set; }
    public string Id1 { get; set; }
    public string Id2 { get; set; }
    public string AveragePieceWeight { get; set; }
    public string TareWeight { get; set; }
    public string PieceWeightOptimisation { get; set; }
    public string PieceWeightTolerancePlus { get; set; }
    public string PieceWeightToleranceMinus { get; set; }
    public string ReferenceSize { get; set; }

    public string Text1 { get; set; }
    public string Text2 { get; set; }

    public BbkArticle(DataPlusResponseArticle dataPlusResponseArticle)
    {
        Name = dataPlusResponseArticle.ArticleId;
        Number = dataPlusResponseArticle.ArticleId;
        Id1 = dataPlusResponseArticle.Info1;
        Id2 = dataPlusResponseArticle.Info2;
        AveragePieceWeight = dataPlusResponseArticle.CountingApw.Replace("\"", "");
        TareWeight = dataPlusResponseArticle.TareValue.Replace("\"", "");
        PieceWeightOptimisation = dataPlusResponseArticle.CountingApwOptimization;
        PieceWeightTolerancePlus = dataPlusResponseArticle.CountingTolerancePlus;
        PieceWeightToleranceMinus = dataPlusResponseArticle.CountingToleranceMinus;
        ReferenceSize = dataPlusResponseArticle.CountingReferenceSize.Replace("\"","");
        Text1 = dataPlusResponseArticle.Info1;
        Text2 = dataPlusResponseArticle.Info2;
    }

    public BbkArticle()
    {
        
    }

}

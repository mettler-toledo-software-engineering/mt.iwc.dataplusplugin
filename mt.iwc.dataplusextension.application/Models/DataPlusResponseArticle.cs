﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.iwc.dataplusextension.application.Models
{
    public class DataPlusResponseArticle
    {
        public string ArticleId { get; }
        public string Description { get;  }
        public string Info1 { get;  }
        public string Info2 { get;  }
        public string Info3 { get;  }
        public string PrimaryUnit { get;  }
        public string TareDescription { get; }
        public string TareValue { get;  }
        public string TotalWeight { get;  }
        public string TotalCount { get;}

        public string CountingUnitType { get;}
        public string CountingUnitName { get; }
        public string CountingUnitFormat { get; }
        public string CountingApw { get; }
        public string CountingApwUnit { get; }
        public string CountingApwOptimization { get; }
        public string CountingApwOptimizationUnit { get; }
        public string CountingReferenceSize { get; }
        public string CountingReferenceSizeUnit { get; }
        public string CountingUnitWeightTolerance { get; }
        public string CountingTolerancePlus { get; }
        public string CountingTolerancePlusUnit { get; }
        public string CountingToleranceMinus { get; }
        public string CountingToleranceMinusUnit { get; }

        public DataPlusResponseArticle(string data)
        {
            //DBC A "10" "Test" "info1" "info2" "" "0.0" "g" "PieceCounting" "PCS" "2" "2" "g" "0.0" "%" "10" "PCS" "Absolute" "0.0" "g" "0.0" "g" "CheckWeighing" "Absolute" "0.0" "PCS" "0.0" "PCS" "0.0" "PCS" "LotN" "0.0" "PCS" "0.0" "g" "0.0" "g" "0.0" "PCS" "0.0" 96
            //"DBC A \"10A\" \"Test\" \"info1\" \"info2\" \"info3\" \"0.0\" \"g\" \"PieceCounting\" \"PCS\" \"2.34\" \"2.34\" \"g\" \"0.5\" \"%\" \"100\" \"PCS\" \"Absolute\" \"0.0\" \"g\" \"0.0\" \"g\" \"CheckWeighing\" \"Absolute\" \"0.0\" \"PCS\" \"0.0\" \"PCS\" \"0.0\" \"PCS\" \"LotN\" \"0.0\" \"PCS\" \"0.0\" \"g\" \"0.0\" \"g\" \"0.0\" \"PCS\" \"0.0\" 225\r\n"
            string[] entries = data.Split(" ");

            ArticleId = entries[2];
            Description = entries[3];
            Info1 = entries[4];
            Info2 = entries[5];
            Info3 = entries[6];
            TareValue = entries[7];
            PrimaryUnit = entries[8];
            
            CountingUnitType = entries[9];
            CountingUnitName = entries[10];
            
            CountingApw = entries[11];
            CountingApwUnit = entries[13];
            CountingApwOptimization = entries[14];
            CountingApwOptimizationUnit = entries[15];
            CountingReferenceSize = entries[16];
            CountingReferenceSizeUnit = entries[17];


        }

    }
}

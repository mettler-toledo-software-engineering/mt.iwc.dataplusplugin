﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.iwc.dataplusextension.application.Models;

public enum ScaleType
{
    BBK,
    ICS
}
public class ConnectionConfiguration
{
    public Guid ConnectionId { get; set; }
    public string? ScaleName { get; set; }
    public string? ScaleComPort { get; set; }
    public string? DataPlusComPort { get; set; }
    public ScaleType Scale { get; set; }
}

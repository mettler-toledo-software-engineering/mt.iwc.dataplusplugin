﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.iwc.dataplusextension.application.Models
{
    public enum DataPlusExtensionWindowsServiceState
    {
        Starting,
        Stopping,
        Running,
        Stopped,
        Paused,
        NotFound,
    }

    public class DataPlusExtensionWindowsService
    {
        public static string SerivceName = "DataPlusExtensionService";
        public DataPlusExtensionWindowsServiceState ServiceState { get; set; }
        public List<ScaleToDataPlusConnection> BbkToDataPlusConnections = new();

    }
}

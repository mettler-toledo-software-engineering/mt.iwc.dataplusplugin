﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.iwc.dataplusextension.application.Services.Communication;

namespace mt.iwc.dataplusextension.application.Models;

public class ScaleToDataPlusConnection
{

    public ConnectionConfiguration? Configuration { get; set; }
    public ConnectionState ConnectionState { get; set; }
    public ISerialToSerialConnectionService SerialConnection { get; set; }
}

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using mt.iwc.dataplusextension.application.Services.Communication;
using Serilog;
using mt.iwc.dataplusextension.windowsservice;
using mt.iwc.dataplusextension.windowsservice.Hubs;
using mt.iwc.dataplusextension.windowsservice.Stores;
using mt.iwc.dataplusextension.application.Persistence;
using mt.iwc.dataplusextension.application.Services.Factories;
using mt.iwc.dataplusextension.application.Services.Translation;
using ILogger = Serilog.ILogger;

//to install service, open powershell as admin
//sc.exe create SerivceName binpath= c:\pathto\serivcename.exe start= auto

//to delete service
//sc.exe delete ServiceName



IHost host = Host.CreateDefaultBuilder(args)
    .UseWindowsService()
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices(services =>
        {
            services.AddSignalR();
            services.AddHostedService<Worker>();
            services.AddSingleton<ISerialToSerialConnectionFactory, SerialToSerialConnectionFactory>();
            services.AddSingleton<IScaleConnectionJsonService, ScaleConnectionJsonService>();
            services.AddSingleton<ScaleConnectionStore>();

        });
        webBuilder.Configure(app =>
        {
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<DataPlusExtensionHub>("/dataPlusExtension");
            });
        });
    })
    .UseSerilog((_, loggerConfiguration) =>
    {
        loggerConfiguration
            .WriteTo.File(@"c:\DataPlusExtension\Logging\Server_Log_.txt", rollingInterval: RollingInterval.Day)
            .WriteTo.Debug()
            .MinimumLevel.Information();
    })
    .Build();

ILogger logger = host.Services.GetRequiredService<ILogger>();

try
{
    await host.RunAsync();
    
}
catch (Exception e)
{
    logger.Fatal(e, "The Service could not be started");
}




﻿
using mt.iwc.dataplusextension.application.Models;
using mt.iwc.dataplusextension.application.Persistence;
using mt.iwc.dataplusextension.application.Services.Communication;
using Microsoft.AspNetCore.SignalR;
using mt.iwc.dataplusextension.application.Services.ClientServerMethods;
using mt.iwc.dataplusextension.application.Services.Factories;
using mt.iwc.dataplusextension.windowsservice.Hubs;

namespace mt.iwc.dataplusextension.windowsservice.Stores;

//todo berücksichtigen, dass wagen offline genommen und wieder neu aufgebaut werden
//todo regelmässig verbindungstatus prüfen und unterbrochene verbindungen regelmässig versuchen neu aufzubauen
//todo prüfen ob alle Einträge in Json in der liste der verbundenen verbindungen sind
//todo alle einträge verbinden, die in json sind aber nicht 
public class ScaleConnectionStore
{
    private List<ScaleToDataPlusConnection> _activeConnections = new ();
    private readonly Serilog.ILogger _logger;
    private readonly IScaleConnectionJsonService _jsonService;
    private readonly ISerialToSerialConnectionFactory _scaleConnectionFactory;
    private readonly IHubContext<DataPlusExtensionHub, IClientMethods> _hub;

    public IEnumerable<ScaleToDataPlusConnection> ActiveConnections => _activeConnections;


    public ScaleConnectionStore(Serilog.ILogger logger, IScaleConnectionJsonService jsonService, ISerialToSerialConnectionFactory scaleConnectionFactory, IHubContext<DataPlusExtensionHub, IClientMethods> hub)
    {
        _logger = logger;
        _jsonService = jsonService;
        _scaleConnectionFactory = scaleConnectionFactory;
        _hub = hub;
    }

    public void EstablishConnections()
    {
        var configurations = _jsonService.LoadScaleConnections();

        foreach (var configuration in configurations)
        {
            ScaleToDataPlusConnection result = new();
            try
            {
                ISerialToSerialConnectionService connection = _scaleConnectionFactory.CreateConnectionService();
                result = connection.ConnectScaleToDataPlus(configuration);

                _activeConnections.Add(result);
                _hub.Clients.All.GetServerStatus($"Waage {result.Configuration?.ScaleName} erfolgreich verbunden");
                _logger.Information($"Waage {result.Configuration?.ScaleName} erfolgreich verbunden");


            }
            catch (Exception e)
            {
                _hub.Clients.All.GetServerStatus($"Waage {result.Configuration?.ScaleName} konnte nicht verbunden werden");
                _logger.Error(e, $"Waage {result.Configuration?.ScaleName} konnte nicht verbunden werden");
            }
        }
    }

    public List<ScaleToDataPlusConnection> TestActiveConnections()
    {
        var testedconnection = new List<ScaleToDataPlusConnection>();
        foreach (ScaleToDataPlusConnection activeConnection in _activeConnections)
        {
            try
            {
                //activeConnection.ConnectionState = ConnectionState.Connected;
                activeConnection.ConnectionState = activeConnection.SerialConnection.CheckConnectionState();
                testedconnection.Add(activeConnection);
            }
            catch (Exception e)
            {
                _hub.Clients.All.GetServerStatus($"Waage {activeConnection.Configuration?.ScaleName} verbindung unterbrochen / dienst neu starten");
                _logger.Error(e, $"Waage {activeConnection.Configuration?.ScaleName} verbindung unterbrochen / dienst neu starten");
            }
           
        }

        if (testedconnection.Count > 0)
        {
            _hub.Clients.All.GetServerStatus($"{testedconnection.Count(x => x.ConnectionState == ConnectionState.Connected)} von {testedconnection.Count} Verbindungen aktiv.");
        }
        
        return testedconnection;
    }

    public void DisposeAllConnections()
    {
        foreach (var activeConnection in _activeConnections)
        {
            activeConnection.SerialConnection.Dispose();
        }
        _activeConnections.Clear();
    }
}

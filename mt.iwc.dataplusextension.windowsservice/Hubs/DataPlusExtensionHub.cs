﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using mt.iwc.dataplusextension.application.Services.ClientServerMethods;

namespace mt.iwc.dataplusextension.windowsservice.Hubs;

public class DataPlusExtensionHub : Hub<IClientMethods>, IServerMethods
{
    public override async Task OnConnectedAsync()
    {
        await Clients.Caller.GetServerStatus("Client verbunden");

    }


    //hier müssen alle methoden definiert werden, welche vom client ausgeführt werden können
    //im IClientMethods Interface können alle Methoden definiert werden, welche der Server auf den Clients ausführen kann.


    //vom server können selbstständig alive zustände der verbindungen geschickt werden
    //vom server könnne selbstständig Log Messages an den Client zugestellt und dargestellt werden.

    //client kann verbindung hinzufügen
    //client kann verbindung löschen
    //client kann verbindung aktualisieren
    //client kann alive status abfragen
    //client kann verbindungsauf auslösen

    //public async Task ClientRequestWithData(string data)
    //{
    //    Debug.WriteLine("testdata received");
    //}

}

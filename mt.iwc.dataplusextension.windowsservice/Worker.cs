using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using mt.iwc.dataplusextension.application.Services.ClientServerMethods;
using mt.iwc.dataplusextension.windowsservice.Hubs;
using mt.iwc.dataplusextension.windowsservice.Stores;

namespace mt.iwc.dataplusextension.windowsservice;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;
    private readonly IHubContext<DataPlusExtensionHub, IClientMethods> _hub;
    private readonly ScaleConnectionStore _connectionStore;


    public Worker(ILogger<Worker> logger, IHubContext<DataPlusExtensionHub, IClientMethods> hub, ScaleConnectionStore connectionStore)
    {
        _logger = logger;
        _hub = hub;
        _connectionStore = connectionStore;
    }



    public override Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("starting up the service");
        _connectionStore.EstablishConnections();
        return base.StartAsync(cancellationToken);
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        _connectionStore.DisposeAllConnections();
        _logger.LogInformation("the service has been stopped");
        return base.StopAsync(cancellationToken);
    }


    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            
            var result =  _connectionStore.TestActiveConnections();
            foreach (var connection in result)
            {
                if (connection.Configuration != null)
                {
                    await _hub.Clients.All.GetConnectionState(connection.ConnectionState, connection.Configuration.ConnectionId);
                }
                
            }
            await Task.Delay(10000, stoppingToken);
        }
    }
}
